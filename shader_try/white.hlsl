Texture2D texture0 : register(t0);

SamplerState sampler0 : register(s0);

cbuffer CB : register(b1)
{
	float strength;
	float reserved1;
	float reserved2;
	float reserved3;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float4 color : COLOR0;
};

float4 PS(VS_OUTPUT input) : SV_Target
{
	float4 srcColor = texture0.Sample(sampler0, input.tex);

	srcColor.rgb += (float3(1.0, 1.0, 1.0) - srcColor.rgb) * strength;
//	srcColor.a -= srcColor.a * strength;
	return srcColor * input.color;

}