﻿#include <Siv3D.hpp>

struct CB1
{
	float strength;
	float reserved1;
	float reserved2;
	float reserved3;
};



void Main()
{
	const String s3dkunBase64Image = L"iVBORw0KGgoAAAANSUhEUgAAADAAAAAgCAMAAABjCgsuAAAARVBMVEUAAACfz7c/n29/v5//x5NwcHDf7+f/ll9QUFC/f/+/38+QkJB/AADPj0/fv//Pn///jIz+AAD////g4ODQ0NDAwMDAAADbxb57AAAAAXRSTlMAQObYZgAAAVlJREFUOMu1kNuWwiAMRRMChulFrbf//9Q54WLBocunOUtpu3eSQuk/wsjEPOAjwSmn0+nD8FhklIz9Gj4WeQiSb6aW96LnSJ00Jz4Slc/BxFw2EMgiyEgw+DwHh0fhEkpxbigEPEAEqDevHWNhHYaFxVmE6VBUDpCW++P+wB2VHAkgxD1vi95VbwvTF5GH6G17KbK9mBohAwGOev9cFo8sC7UNBwLYX6+gdqFdyFg4xKdsm/cA30XOumKhjo+FHUKwyCrAbcYCuERFOiOt+KznaWKk63CdeGOcSVzik3HfnaGKvkExinNE2wbMckU0XLVtWLU5A2bJ3waH7A3tmYPq3qANdyli37D/SGPBbOKnpH3BWAAbP58vF/uHEOr8XuRiklK/pj2q1xCjmV6oat0MV+wTsfrY13uYtCQMChxj3UetD1kQRQ1w0dedZkrvBirzS7kZpKzH+QXriRABIX6TjgAAAABJRU5ErkJggg==";

	const Texture texture(Base64::Decode(s3dkunBase64Image));

	PixelShader
		ps(L"./white.hlsl")
		, gray_{L"./gray.psh"}
		, black_{L"./black.psh"}
		, red_{ L"./red.psh" }
		, blue_{L"./blue.psh"}
		, green_{L"./green.psh"}
		, alpha_{ L"./transparent.psh" }
		, invert_{L"./invert.psh"}
	;
	

	//PixelShader ps(L"./Example/Shaders/Invert2D.hlsl");


	if (ps.isEmpty()) {
		s3d::Console::Open();
		std::cout << "empty" << (ps.isEmpty() ? "ps" : "tr") << "\n";
		int x; std::cin >> x;
		s3d::Console::Close();
		throw;
	}
	

	ConstantBuffer<CB1> cb1, cb2;

	Graphics::SetBackground(Color(40));

	Graphics2D::SetSamplerState(SamplerState::ClampPoint);

	Window::Resize(800, 600);


	s3d::GUI gui{ s3d::GUIStyle::Default };
	[&gui]() {
		gui.setTitle(L"controller");
		gui.add(L"rd", GUIRadioButton::Create(
			 {L"White",L"Gray",L"Black",L"Red",L"Green",L"Blue",L"Alpha"}, 0u, true)
		);

		gui.setPos(300, 0);
	}();//gui configuration;

	while (System::Update())
	{
		const int F = System::FrameCount();
		/*
			sin => [-1,1]
			0.5 * sin => [-0.
			dldldda
			5, 0.5]
			0.5 * sin + 0.5 => [0, 1.0]
		*/
		const auto easing = (float)Saturate(sin(F / 20.0) * 0.5 + 0.5);


		Graphics2D::BeginPS(ps);

		cb1->strength = easing;


		Graphics2D::SetConstant(ShaderStage::Pixel, 1, cb1);

		auto x = ((F / 25) % 2) * texture.width / 2;
		auto y = 0;
		auto w = texture.width / 2;
		auto h = texture.height;
	
		texture(x, y, w, h).scale(3).draw(100, 100);
		Graphics2D::EndPS();

//Right image

		auto checked = gui.radioButton(L"rd").checkedItem;
		s3d::Optional<decltype(ps)> psh;
		if (!checked)
			psh = ps;
		else
			switch (*checked)
			{
				case 1:
					psh = gray_; break;

				case 2:
					psh = black_; break;

				case 3:
					psh = red_; break;

				case 4:
					psh = green_; break;
				case 5:
					psh = blue_; break;
				case 6:
					psh = alpha_; break;

				default:
					psh = ps;

			}


		Graphics2D::BeginPS(*psh);

		cb2->strength = easing;
		
		Graphics2D::SetConstant(ShaderStage::Pixel, 1, cb2);
		texture(x, y, w, h).scale(3).draw(200,100);

		Graphics2D::EndPS();

		ClearPrint();
		Println(L"w-stren = ", cb1->strength);
		Println(L"t-stren = ", cb2->strength);
		Println(L"radio_button ", gui.radioButton( L"rd" ).checkedItem );
	}
}